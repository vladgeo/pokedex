const initialURL = 'https://pokeapi.co/api/v2/pokemon'

export function getData(url) {
    return new Promise((resolve, reject) => {
        fetch(url).then(r => r.json())
            .then(list => {
                Promise
                    .all(list.results.map(item => fetch(item.url)))
                    .then(responses => Promise
                        .all(responses.map(response => response.json())))
                    .then(data => {
                        const mergeData = [];
                        for (let i = 0; i < list.results.length; i++) {
                            mergeData.push({
                                ...list.results[i],
                                ...data.find(d => d.name === list.results[i].name)
                            })
                        }
                        list.results = mergeData;
                        resolve(list);
                    })
            })
    })
}

export const getPokemonDetails = async (id) => {
    const request = await fetch(`${initialURL}/${id}`);
    return request.json();
}