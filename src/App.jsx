import React from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import List from './components/List'
import Details from './components/Details/Details'
import NotFound from './components/NotFound/NotFound';
import Footer from './components/Footer/Footer';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';


function App() {
    return (
        <Router>
            <Navbar/>
            <Switch>
                <Route exact path='/' component={List}/>
                <Route path='/p/:page' component={List}/>
                <Route path='/details/:id' component={Details}/>
                <Route component={NotFound}/>
            </Switch>
            <Footer/>
        </Router>
    );
}

export default App;