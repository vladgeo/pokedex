import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom'
import {getPokemonDetails} from '../../services/pokemon';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        borderRadius: 'none',
        boxShadow: 'none',
        margin: '0 auto',
        textAlign: 'center',
        paddingBottom: '6vh',
    },
    image: {
        width: 128,
        height: 96,
        margin: '0 auto'
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
    capitalize: {
        textTransform: 'capitalize'
    },
    evoImg: {
        display: 'flex',
        margin: '0 auto',
    }
}));

const Details = ({history}) => {
    let {id} = useParams();
    const [pokemonData, setPokemonData] = useState(null);

    function fetchData() {
        getPokemonDetails(id)
            .then(data => {
                setPokemonData(data)
            })
    }

    useEffect(() => {
        fetchData();
        // eslint-disable-next-line
    }, [])

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <h3>Details page</h3>
                            <h4 className={classes.capitalize}>
                                {pokemonData && pokemonData.name}
                            </h4>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <div>
                                <img className={classes.img} alt={pokemonData && pokemonData.name}
                                     src={pokemonData && pokemonData.sprites.front_default}/>
                            </div>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Grid item xs={4} className={classes.evoImg}>
                                <div className={classes.image}>
                                    <img className={classes.img} alt={pokemonData && pokemonData.name}
                                         src={pokemonData && pokemonData.sprites.back_default}/>
                                </div>
                                <div className={classes.image}>
                                    <img className={classes.img} alt={pokemonData && pokemonData.name}
                                         src={pokemonData && pokemonData.sprites.front_shiny}/>
                                </div>
                                <div className={classes.image}>
                                    <img className={classes.img} alt={pokemonData && pokemonData.name}
                                         src={pokemonData && pokemonData.sprites.back_shiny}/>
                                </div>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper className={classes.paper}>
                            <Typography className={classes.capitalize}>
                                Name: {pokemonData && pokemonData.name}
                            </Typography>
                            <Typography>
                                Type: {pokemonData && pokemonData.types[0].type.name}
                            </Typography>
                            <Typography>
                                Ability: {pokemonData && pokemonData.abilities[0].ability.name}
                            </Typography>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Paper className={classes.paper}>
                            <Typography>
                                ID: {pokemonData && pokemonData.id}
                            </Typography>
                            <Typography>
                                Weigth: {pokemonData && pokemonData.weight}
                            </Typography>
                            <Typography>
                                Height: {pokemonData && pokemonData.height}
                            </Typography>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            style={{margin: '1rem'}}
                            variant='contained'
                            color='secondary'
                            onClick={() => history.goBack()}
                        >
                            go back
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    );
}
export default Details;