import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    footer: {
        margin: '0px auto',
    },
});

export default function Footer() {
    const classes = useStyles();

    return (
        <AppBar position='static' color='primary'>
            <Container maxWidth='md'>
                <Toolbar>
                    <Typography
                        variant='body1'
                        color='inherit'
                        className={classes.footer}
                    >
                        © {new Date().getFullYear()} Copyright
                    </Typography>
                </Toolbar>
            </Container>
        </AppBar>
    )
}