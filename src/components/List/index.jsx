import React, {useEffect, useState} from 'react';
import {getData} from '../../services/pokemon';
import LinearProgress from '@material-ui/core/LinearProgress';
import Pagination from '@material-ui/lab/Pagination';
import Card from '../Card/Card';
import {makeStyles} from '@material-ui/core/styles';
import {useParams} from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
}));

const List = ({history}) => {
    const [pokemonData, setPokemonData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [numberOfPages, setnumberOfPages] = useState(1);
    const initialURL = 'https://pokeapi.co/api/v2/pokemon'
    const itemsPerPage = 20;
    const {page = 1} = useParams();
    const p = parseInt(page);
    if (isNaN(p)) {
        history.replace('/NotFound');
    }

    function fetchData(url) {
        setLoading(true);
        getData(url || initialURL)
            .then(data => {
                const numberOfPages = data ? Math.floor(data.count / itemsPerPage) : 0
                if (numberOfPages < p || p < 1) {
                    history.replace('/NotFound');
                }
                setnumberOfPages(numberOfPages);
                setPokemonData(data)
            })
        setLoading(false);
    }

    useEffect(() => {
        const offSet = (p - 1) * itemsPerPage;
        const url = initialURL + `/?limit=${itemsPerPage}&offset=${offSet}`;
        fetchData(url);
        // eslint-disable-next-line
    }, [p])

    const handlePaginationOnChange = (event, _p) => {
        history.push(`/p/${_p}`);
    }

    const classes = useStyles();

    return (
        <div>
            {loading ?
                <div className={classes.root}>
                    <LinearProgress/>
                </div> : (
                    <>
                        <div className='button-block'>
                            <Pagination
                                count={numberOfPages}
                                color='primary'
                                page={p}
                                onChange={handlePaginationOnChange}
                            />
                        </div>
                        <div className='grid-container'>
                            {pokemonData && pokemonData.results.map((pokemon, i) => {
                                return <Card key={i} pokemon={pokemon}/>
                            })}
                        </div>
                        <div className='button-block'>
                            <Pagination
                                count={numberOfPages}
                                color='primary'
                                page={p}
                                onChange={handlePaginationOnChange}
                            />
                        </div>
                    </>
                )}
        </div>
    )
}
export default List;