import React from 'react';
import typeColors from '../../helpers/typeColors'
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {withRouter} from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        maxWidth: 400,
    },
    media: {
        height: 160,
    },
    imgPokemon: {
        width: '50%',
        margin: '0px auto',
        display: 'flex',
        paddingTop: '1rem',
    },
});

const MediaCard = ({pokemon, history}) => {
    const classes = useStyles();
    const getImageCard = () => {
        return pokemon.sprites ? pokemon.sprites.front_default : ''
    }

    console.log(pokemon);
    const getFirstPokemonItem = (pokemonList) => {
        let pokemonObj = {name: '', color: 'transparent'};

        if (pokemonList && pokemonList[0]) {
            pokemonObj.name = pokemonList[0].type.name;
            pokemonObj.color = typeColors[pokemonObj.name];
        }
        return pokemonObj;
    }

    const handleCardClick = () => {
       history.push(`/details/${pokemon.id}`);
    }

    return (
        <Card className={classes.root} onClick={handleCardClick}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image=''
                    title={pokemon.name}
                >
                    <div>
                        <img
                            className={classes.imgPokemon}
                            src={getImageCard()}
                            alt={pokemon.name}
                        />
                    </div>
                </CardMedia>
                <CardContent>
                    <Typography
                        gutterBottom variant='h5'
                        component='h2'
                        style={{textTransform: 'capitalize', fontSize: '1.2rem', textAlign: 'center'}}
                    >
                        {pokemon.name}
                    </Typography>
                    <Typography
                        variant='body2'
                        color='textSecondary'
                        style={{textAlign: 'center', backgroundColor: getFirstPokemonItem(pokemon.types).color}}
                    >
                        {getFirstPokemonItem(pokemon.types).name}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export default withRouter(MediaCard);