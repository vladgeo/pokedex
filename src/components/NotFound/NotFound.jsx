import React from 'react';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 3),
        margin: '2.1rem auto',
        textAlign: 'center',
    },
    paper: {
        margin: `${theme.spacing(11)}px auto`,
        padding: theme.spacing(2),
    },
}));

export default function NotFound() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container wrap='nowrap' spacing={2}>
                    <Grid item xs>
                        <Grid item>
                            <Avatar style={{backgroundColor: '#f50057'}}>404</Avatar>
                        </Grid>
                        <h1 style={{
                            fontSize: '5rem',
                            color: '#f50057'
                        }}>404</h1>
                        <Typography>
                            The page you are looking for was not found.
                        </Typography>
                    </Grid>
                </Grid>
                <Grid item xs>
                    <Link to='/' style={{textDecoration: 'none'}}>
                        <Button variant='contained'
                                color='secondary'
                                style={{margin: '3rem auto'}}
                        >
                            back to home
                        </Button>
                    </Link>
                </Grid>
            </Paper>
        </div>
    );
}