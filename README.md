# Pokédex React App

This project was made with:
ReactJS, react-router-dom, Material-UI, RESTful Pokémon API.

The working version of the application can be viewed at this link [Click here](https://pokedex-vladgeo.vercel.app/).